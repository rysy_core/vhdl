vlib work

vcom gates.vhd
vcom gates_tb.vhd

vsim work.gates_tb

add wave sim:/gates_tb/dut/clk
add wave sim:/gates_tb/dut/a
add wave sim:/gates_tb/dut/b
add wave sim:/gates_tb/dut/a_r
add wave sim:/gates_tb/dut/b_r
add wave sim:/gates_tb/dut/l1
add wave sim:/gates_tb/dut/l2

run 120ns

wave zoom full


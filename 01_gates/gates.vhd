library ieee;
use ieee.std_logic_1164.all;

entity gates is
    port (
        clk     : in std_logic;
        a, b    : in std_logic;
        l1, l2  : out std_logic
    );
end entity gates;

architecture rtl of gates is
    signal a_r, b_r : std_logic;
begin
    process (clk)
    begin
        if rising_edge(clk) then
            a_r <= a;
            b_r <= b;
            l1 <= a_r and b_r;
            l2 <= a_r or b_r;
        end if;
    end process;
end architecture rtl;


library ieee;
use ieee.std_logic_1164.all;

entity gates_tb is
end entity gates_tb;

architecture rtl of gates_tb is
   signal tb_clk, tb_a, tb_b, tb_l1, tb_l2 : std_logic;
begin

    dut : entity work.gates PORT MAP (
        clk => tb_clk,
        a => tb_a,
        b => tb_b,
        l1 => tb_l1,
        l2 => tb_l2);

    process
    begin
        tb_clk <= '0';
        wait for 10 ns;
        tb_clk <= '1';
        wait for 10 ns;
    end process;

    tb_a <= '0',
            '1' after 20 ns,
            '0' after 40 ns,
            '1' after 60 ns;
    tb_b <= '0',
            '1' after 40 ns;

end architecture rtl;

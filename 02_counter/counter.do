vlib work

vcom ../utils.vhd
vcom counter.vhd
vcom counter_tb.vhd

vsim work.counter_tb

add wave sim:/counter_tb/dut/clk
add wave sim:/counter_tb/dut/rst
add wave sim:/counter_tb/dut/ce
add wave -unsigned sim:/counter_tb/dut/q
add wave sim:/counter_tb/dut/ov

run 300ns

wave zoom full


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.utils.clog2;

entity counter is
    generic (N : integer := 16);
    port (
        clk, rst, ce : in std_logic;
        q : out std_logic_vector (clog2(N)-1 downto 0);
        ov : out std_logic
    );
end entity counter;

architecture rtl of counter is
    signal q_r : std_logic_vector (clog2(N)-1 downto 0);
    signal ov_r : std_logic;
begin

    process (clk, rst)
    begin
        if rst = '0' then
            q_r <= (others => '0');
            ov_r <= '0';
        elsif rising_edge(clk) then
            if ce = '1' then
                if ov_r = '1' then
                    q_r <= (others => '0');
                else
                    q_r <= q_r + 1;
                end if;

                if q_r = N-2 then
                    ov_r <= '1';
                else
                    ov_r <= '0';
                end if;
            end if;
        end if;
    end process;

    q <= q_r;
    ov <= ov_r;

end architecture rtl;


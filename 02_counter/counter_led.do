vlib work

vcom ../utils.vhd
vcom counter.vhd
vcom counter_led.vhd
vcom counter_led_tb.vhd

vsim work.counter_led_tb

add wave sim:/counter_led_tb/dut/clk
add wave sim:/counter_led_tb/dut/rst
add wave -format analog-step -unsigned -height 100 \
    -min -0 -max 8000000 sim:/counter_led_tb/dut/cnt_1s/q
add wave sim:/counter_led_tb/dut/ov
add wave -unsigned sim:/counter_led_tb/dut/led

run 15000ms

wave zoom full


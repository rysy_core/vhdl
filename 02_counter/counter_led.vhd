library ieee;
use ieee.std_logic_1164.all;
use work.utils.clog2;

entity counter_led is
    port (
        clk, rst : in std_logic;
        led : out std_logic_vector (3 downto 0)
    );
end entity counter_led;

architecture rtl of counter_led is
    component counter
        generic (N : integer := 16);
        port (
            clk, rst, ce : in std_logic;
            q : out std_logic_vector (clog2(N)-1 downto 0);
            ov : out std_logic
        );
    end component;
    for all : counter use entity work.counter(rtl);
    signal ov : std_logic;
begin

    cnt_1s : counter
        generic map (N => 8000000)
        port map (
            clk => clk,
            rst => rst,
            ce => '1',
            q => open,
            ov => ov);

    cnt_led : counter
        generic map (N => 10)
        port map (
            clk => clk,
            rst => rst,
            ce => ov,
            q => led,
            ov => open);

end architecture rtl;


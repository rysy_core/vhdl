library ieee;
use ieee.std_logic_1164.all;

entity counter_led_tb is
end entity counter_led_tb;

architecture rtl of counter_led_tb is
   signal clk, rst : std_logic;
begin

    dut : entity work.counter_led
        port map (
            clk => clk,
            rst => rst,
            led => open);

    process
    begin
        clk <= '0';
        wait for 62.5 ns;
        clk <= '1';
        wait for 62.5 ns;
    end process;

    rst <= '0',
           '1' after 100 ns;

end architecture rtl;


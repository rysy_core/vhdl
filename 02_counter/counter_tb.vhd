library ieee;
use ieee.std_logic_1164.all;

entity counter_tb is
end entity counter_tb;

architecture rtl of counter_tb is
   signal clk, rst, ce : std_logic;
begin

    dut : entity work.counter
        generic map (N => 10)
        port map (
            clk => clk,
            rst => rst,
            ce => ce,
            q => open,
            ov => open);

    process
    begin
        clk <= '0';
        wait for 10 ns;
        clk <= '1';
        wait for 10 ns;
    end process;

    rst <= '0',
           '1' after 30 ns;
    ce <= '1',
          '0' after 100 ns,
          '1' after 140 ns;

end architecture rtl;


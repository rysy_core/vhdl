vlib work

vcom ../utils.vhd
vcom ../02_counter/counter.vhd
vcom seg7.vhd
vcom seg7_tb.vhd

vsim work.seg7_tb

add wave sim:/seg7_tb/clk
add wave sim:/seg7_tb/rst
add wave -unsigned sim:/seg7_tb/dut/d
add wave sim:/seg7_tb/dut/q

run 300ns

wave zoom full


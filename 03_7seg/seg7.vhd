library ieee;
use ieee.std_logic_1164.all;

entity seg7 is
    port (
        clk : in std_logic;
        d : in std_logic_vector (3 downto 0);
        q : out std_logic_vector (6 downto 0)
    );
end entity seg7;

architecture rtl of seg7 is
begin

    process (clk)
    begin
        if rising_edge(clk) then
            case d is
                when "0000" => q <= "1000000";
                when "0001" => q <= "1111001";
                when "0010" => q <= "0100100";
                when "0011" => q <= "0110000";
                when "0100" => q <= "0011001";
                when "0101" => q <= "0010010";
                when "0110" => q <= "0000010";
                when "0111" => q <= "1111000";
                when "1000" => q <= "0000000";
                when "1001" => q <= "0010000";
                when others => q <= "1111111";
            end case;
        end if;
    end process;

end architecture rtl;


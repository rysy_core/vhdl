library ieee;
use ieee.std_logic_1164.all;
use work.utils.clog2;

entity seg7_led is
    port (
        clk, rst : in std_logic;
        led : out std_logic_vector (6 downto 0)
    );
end entity seg7_led;

architecture rtl of seg7_led is
    component counter
        generic (N : integer := 16);
        port (
            clk, rst, ce : in std_logic;
            q : out std_logic_vector (clog2(N)-1 downto 0);
            ov : out std_logic
        );
    end component;
    for all : counter use entity work.counter(rtl);

    component seg7
        port (
            clk : in std_logic;
            d : in std_logic_vector (3 downto 0);
            q : out std_logic_vector (6 downto 0)
        );
    end component;
    for all : seg7 use entity work.seg7(rtl);

    signal ov : std_logic;
    signal d : std_logic_vector (3 downto 0);
begin

    cnt_1s : counter
        generic map (N => 8000000)
        port map (
            clk => clk,
            rst => rst,
            ce => '1',
            q => open,
            ov => ov);

    cnt_led : counter
        generic map (N => 10)
        port map (
            clk => clk,
            rst => rst,
            ce => ov,
            q => d,
            ov => open);

    seg : seg7
        port map (
            clk => clk,
            d => d,
            q => led
        );

end architecture rtl;


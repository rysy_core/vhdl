library ieee;
use ieee.std_logic_1164.all;

entity seg7_tb is
end entity seg7_tb;

architecture rtl of seg7_tb is
    signal clk, rst : std_logic;
    signal d : std_logic_vector (3 downto 0);
begin

    cnt : entity work.counter
        generic map (N => 10)
        port map (
            clk => clk,
            rst => rst,
            ce => '1',
            q => d,
            ov => open);

    dut : entity work.seg7
        port map (
            clk => clk,
            d => d,
            q => open
        );

    process
    begin
        clk <= '0';
        wait for 10 ns;
        clk <= '1';
        wait for 10 ns;
    end process;

    rst <= '0',
           '1' after 10 ns;

end architecture rtl;


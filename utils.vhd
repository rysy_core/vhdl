library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.ceil;
use ieee.math_real.log2;

package utils is

    function clog2( n : natural) return integer;

end utils;

package body utils is

    function clog2( n : natural) return integer is
    begin
        return integer(ceil(log2(real(n))));
    end function;

end utils;

